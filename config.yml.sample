---
#
# Any options in this file overwrite the global 'coriolis_ansible' values from:
#   ./coriolis_ansible/group_vars/all.yml
#   ./coriolis_ansible/roles/<ROLE_NAME>/vars/main.yml
#

kolla_branch: 9.0.0
kolla_openstack_release: train
kolla_docker_images_tag: latest

docker_pull_images: true
coriolis_worker_hostname:
default_coriolis_region_name: Public

enable_coriolis_licensing_server: false

enable_coriolis_compressor: true
compress_transfers: false

coriolis_profiling_dir_host: /opt/coriolis/profiling
# Enabled memory profiling for components using 'fil'
# Profiling data will be saved within subdirs with the same directory name
# as the component within the 'coriolis_profiling_dir_host'
# Note that profiling only happens for the root process of the container,
# so explicitly setting 'messaging_workers = 1' in coriolis.conf is
# recommended for the most accurate results possible.
# The dumping of profiling data must be explicitly tirggered using
# 'coriolis-ansible dump-memory-profiling'
# Possible component labels are:
# ["api", "conductor", "worker", "scheduler", "minion-manager", "replica-cron"]
enable_profiling_for: []

coriolis_export_providers: ["openstack", "oracle-vm", "opc", "azure", "scvmm", "vmware", "aws"]
coriolis_import_providers: ["openstack", "oracle-vm", "opc", "azure", "scvmm", "oci", "aws", "vmware", "ovirt"]

# In the case that some repo needs to be cloned on some other branch instead of
# the default cloudbase/master that can be set using the two following dict
# variables.
# In both of these dicts, the key is expected to be either a provider name
# (like in the export/import_providers lists above) or one of the following:
# "core", "web", "licensing-server", "licensing-ui", "logger".
# Examples:
# {openstack: cloudbase, aws: cloudengineer, etc: etc} - owner dict
# {openstack: master, aws: branch2, etc: etc} - branch dict
custom_repo_owner_names: {}
custom_repo_branch_names: {}

# Set this variable if the ssh-agent isn't set, with the authorized Bitbucket
# SSH private key loaded.
# bitbucket_ssh_key_file: "/root/.ssh/id_rsa"
